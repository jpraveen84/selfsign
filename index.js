const http = require('http');
const fs = require('fs');
const constants = require('./constants');


const server = http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/html'});

    const html = fs.readFileSync('index.html').toString('utf-8')
    // Send the response body "Hello World"
    res.end(html.replace('COLOR', constants.color));
});

server.listen(80, () => console.log('server started at', 80))