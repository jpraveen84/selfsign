# selfsign


## Getting started

In this project we are using gitlab for ci-cd and Terraform to build the AWS infra. AWS ECS is used as a platform to host our containerized Node.js application.

## Why ECS
ECS is a fully managed, highly scalable, high-performance container orchestration service that supports Docker containers and allows you to easily run and scale containerized applications on AWS.

## Authentication

Any CI/CD tool needs to have authentication with the infra to build, provision and deploy the application service. Here we are about to discuss the two authentication approaches to build and deploy the application.

 ### Approach 1 - Local gitlab runner in AWS account


Create an IAM role and attach necessary policies to create infra.


Launch an ec2 instance in AWS and install all the package dependencies (terraform, awscli, docker gitlab runner) and make sure this EC2 instance is  attached with the above IAM role which we created.

Login to the EC2 and run the below command to register the EC2 instance with gitlab as a runner.


- [ ] [install gitlab runner](https://docs.gitlab.com/runner/install/)


```
sudo gitlab-runner register \
  --non-interactive \
  --url $GITLAB_URL \
  --registration-token $REGISTRATION_TOKEN \
  --tag-list "ec2-runner" \
  --executor shell \
  --description "ec2-runner"
```

In the above command replace **GITLAB_URL** and **REGISTRATION_TOKEN** with yours.

You can get the REGISTRATION_TOKEN form gitlab (_Project -> settings -> CI/CD -> Runners_)

![gitrunner](https://gitlab.com/jpraveen84/selfsign/-/blob/main/screenshot/gitlabrunner.png)

In the above command we are registering the EC2 instance with the gitlab project by using the --tag-list flag. If any job needs to consume this runner, then we need to declare this under tag as mentioned below.

```test:
  stage: test
  script:
    - echo "Running tests..."
  rules:
    - if: '$CI_COMMIT_REF_NAME == "main"'
  tags:
    - ec2-runner
```
  

**Note:** **In this approch the IAM role and EC2 which we created not as a part of terraform. We need to create this in AWS web console or aws cli command** 


## Approch 2 - By using a gitlab managed runner

Create an IAM bot user with programmatic access and configure the Access and Secret key in Gitlab managed runner to authenticate the AWS account. In addition to that we need to add two more variables in gitlab,  git lab pat token and runner registration token.
 
![gitvariable](https://gitlab.com/jpraveen84/selfsign/-/blob/main/screenshot/gitvariable.png)

While configuring variables kindly use the same key name which is mentioned below as we use these values in the gitlab-ci yml file .
  ```   
     AWS_ACCESS_KEY —----> Access token
     AWS_SECRET_ACCESS_KEY —----> Secret key
     GIT_RUNNER_TOKEN —-----> Runner registration token
     TOKEN —-----> Gitlab pat token
     USERNAME —---> Gitlab user name
  ```
In this approach we are using gitlab managed runner to create infra and build the application. We are using the local ec2 runner to deploy the application which will get created as a part of terraform.


In this project we are aligned with approach 2 so that all the AWS resources will be tracked in a terraform state file.
.

## Steps to recreate this Gitlab cicd in your infra


Before getting started we need to know about the resources that will be created in your AWS account.

1. VPC (all vpc components such as subnet in all AZ, IGW, NAT, SG)
2. ECR repo to manage the docker images
3. Application Load Balancer and Target Group.
4. ECS Cluster and TASK Definitions
5. EC2 instance (local gitlab runner)
6. IAM role for EC2 instance (to deploy application)
7. Cloudwatch log group
8. ssh key

## Step 1

Create a project(repo) in gitlab and note down the project ID. We need to refer to this project ID in our gitlab ci yml file. 

## Step 2

Clone this repo and do the necessary changes in “iac/demo.tfvars” files. Please double check before editing any values in this file.

## Step 3
Replace the region (please make sure the region name in iac/main.tf and gitlab-ci.yml should be same) and aws account ID in .gitlab-ci.yml with your preferences and push/commit the changes to the main branch in the above created project in gitlab.

```
variables:
  TF_DIR: ${CI_PROJECT_DIR}/iac
  STATE_NAME: "selfsign-tf"          #statefile name
  ADDRESS: "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${STATE_NAME}"
  DOCKER_IMAGE_TAG: ${CI_PIPELINE_ID} 
  USERNAME: ""                       #gitlab user name to access the private token
  APP_NAME: ${CI_PROJECT_NAME}
  AWS_ACCNT_ID: "XXXXXXX"            # aws account ID
  AWS_REGION: "us-east-1"            # region to deploy app
  ECR_REPO: "${AWS_ACCNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com"
```
 
 Once you push the change to the main branch it will deploy both infra and application.


## Step 4

Once the terraform apply command  is completed, it will return a set of outputs, wherein you will get the ALB dns name “alb_dn” . You can access the application using this DNS

## CI/CD

For the CI/CD , if you modify any files in iac folder and commit any changes, gitlab managed runner get established and deploy those changes to infra. Similarly, if you change any value in root document file, the local managed runner deploy the latest version of application to ECS 

For eg : If you modify the file constant.js and commit the changes, gitlab CI will deploy the new version of the app. You can access the application using the ALB which was created in Terraform. 
```
module.exports = {
   color: 'red'
}
```


In the above snippet, if you replace the color value to yellow. You will get the yellow background in webpage after the deployment.

