
locals {
  instance_configs = {
    "gitlab-runner" = {
      ami_id                      = "ami-0aa2b7722dc1b5612"
      instance_type               = var.runner_instance_type
      sg                          = module.security_groups.security_group_ids
      subnet                      = tolist(module.subnet[*].vpc_public_subnets)
      associate_public_ip_address = true
      role                        = module.iam_role.*.ec2runner.roleName[0]
      tags = {
        Name = "Gitlab-Runner"
      }
    }
  }
}

module "ec2_instances" {
  source                      = "./modules/ec2"
  for_each                    = local.instance_configs
  instance_type               = each.value.instance_type
  ami                         = each.value.ami_id
  security_group_ids          = each.value.sg
  key_name                    = var.key_name
  subnet                      = each.value.subnet
  associate_public_ip_address = each.value.associate_public_ip_address
  tags                        = each.value.tags
  iam_instance_profile        = each.value.role
  depends_on                  = [module.iam_role]
}
