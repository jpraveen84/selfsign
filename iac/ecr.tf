locals {
  ecr_repo = {
    "selfsign" = {
      tag_mut = "MUTABLE"
      scan    = false
    }
  }
}

module "ecr-repo" {
  source    = "./modules/ecr"
  for_each  = local.ecr_repo
  repo_name = each.key
  tag_mut   = each.value.tag_mut
  scan      = each.value.scan

}