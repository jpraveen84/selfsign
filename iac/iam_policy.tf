locals {
  policy_name = ["selfsign-ecrpolicy", "selfsign-ecstaskexecutionpolicy"]
}

module "iam_policy" {
  source      = "./modules/iam_policy"
  for_each    = toset(local.policy_name)
  policy_name = each.key
}