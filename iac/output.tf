output "vpc-id" {
  value = toset(module.create_vpc[*].vpcId)
}
output "vpc" {
  value = module.create_vpc[0].vpcId
}

output "public_subnets" {
  #value = module.subnet.vpc_public_subnets
  value = tolist(module.subnet[*].vpc_public_subnets)
}

output "private_subnets" {
  #value = module.subnet.vpc_private_subnets
  value = tolist(module.subnet[*].vpc_private_subnets)

}

output "public" {
  #value = module.subnet.vpc_public_subnets
  value = module.subnet.*.vpc_public_subnets[0]
}
output "private" {
  #value = module.subnet.vpc_private_subnets
  value = module.subnet.*.vpc_private_subnets[0]

}

output "key_pair" {
  value = tolist(module.sshKey[*].keyPair)
}

output "key" {
  value = module.sshKey[0].keyPair
}

output "sg" {
  value = module.security_groups.security_group_ids
}

output "public-IP" {
  value = module.ec2_instances.*.gitlab-runner[0].publicIp[0]
}

output "ecs_clusterID" {
  value = module.ecs_cluster.*.selfsign[0].clusterID
}

output "task_arn" {
  value = module.ecs_tasks.*.selfsign.taskArn[0]
}

output "ecr_policy_arn" {
  value = module.iam_policy.*.selfsign-ecrpolicy[0].policyARN[0]
}

output "ecs_policy_arn" {
  value = module.iam_policy.*.selfsign-ecstaskexecutionpolicy[0].policyARN[0]
}

output "role_name" {
  value = module.iam_role
}

output "role_arn" {
  value = module.iam_role.*.ecs.roleARN[0]
}

output "ec2-role_arn" {
  value = module.iam_role.*.ec2runner.roleARN[0]
}
output "ec2-role_name" {
  value = module.iam_role.*.ec2runner.roleName[0]
}

output "tg_arn" {
  value = module.alb.*.tgARN.arn[0]
}

output "alb_dns" {
  value = module.alb.*.albDNS.dns_name[0]
}