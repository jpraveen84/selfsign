module "alb" {
  source   = "./modules/alb"
  subnets  = module.subnet.*.vpc_public_subnets[0]
  sg_id    = module.security_groups.security_group_ids
  vpc_id   = module.create_vpc[0].vpcId
  app_name = var.app_name
}