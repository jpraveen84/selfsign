locals {
  cluster_name = {
    "selfsign" = {
      capacity_providers = ["FARGATE"]
    }
  }
}



module "ecs_cluster" {
  for_each           = local.cluster_name
  source             = "./modules/ecs_cluster"
  name               = each.key
  capacity_providers = each.value.capacity_providers
}
