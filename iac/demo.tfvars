account_id = "594783389282" #account ID

app_name = "selfsign" #application Name
env      = "Demo"     #env Name


#Create new vpc
create_vpc           = true            # make it false if you dont want to create new vpc
cidr                 = "172.16.0.0/16" # VPC CIDR / if CIDR exist change with new cidr
enable_dns_hostnames = true
enable_dns_support   = true
newbits              = 8    # new host allocation in vpc to create subnets
public_ip            = true # assign public ip to instance in public subnet



#use existing vpc
existing_vpc_id = []
public_subnet   = []
private_subnet  = []
security_groups = []

create_key = true
key_name   = "selfsign"

ssh_port = "22"

ssh_cidr = ["223.178.80.201/32"]

runner_instance_type = "t2.micro"            #git lab local runner instance type
gitlab_url           = "https://gitlab.com/" #git lab url


#ECS cluster details

selfsign = {
  family                   = "service"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
}

#Default image while creating ECS task definition

image_name = "pvermeyden/nodejs-hello-world:a1e8cf1edcc04e6d905078aed9861807f6da0da4"