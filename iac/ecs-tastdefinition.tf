locals {
  task = {
    "selfsign" = {
      family                   = var.selfsign.family
      requires_compatibilities = var.selfsign.requires_compatibilities
      cpu                      = var.selfsign.cpu
      memory                   = var.selfsign.memory
      network_mode             = var.selfsign.network_mode
      image_name               = var.image_name
    }
  }
}



module "ecs_tasks" {
  for_each                 = local.task
  source                   = "./modules/ecs_task"
  service_name             = each.key
  family                   = each.value.family
  requires_compatibilities = each.value.requires_compatibilities
  role                     = module.iam_role.*.ecs.roleARN[0]
  network_mode             = each.value.network_mode
  cpu                      = each.value.cpu
  memory                   = each.value.memory
  image_name               = each.value.image_name
}