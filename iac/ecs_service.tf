locals {
  ecs_service = {
    "selfsign" = {
      ecs_clusterID     = module.ecs_cluster.*.selfsign[0].clusterID
      ecs_taskarn       = module.ecs_tasks.*.selfsign.taskArn[0]
      subnet            = module.subnet.*.vpc_private_subnets[0]
      ecsSG             = module.security_groups.security_group_ids
      tg                = module.alb.*.tgARN.arn[0]
      container_name    = var.app_name
      capacity_provider = "FARGATE"
    }
  }

}



module "ecs_service" {
  source            = "./modules/ecs_service"
  for_each          = local.ecs_service
  app_name          = each.key
  ecs_clusterID     = each.value.ecs_clusterID
  ecs_taskarn       = each.value.ecs_taskarn
  subnet            = each.value.subnet
  ecsSG             = each.value.ecsSG
  tg                = each.value.tg
  container_name    = each.value.container_name
  capacity_provider = each.value.capacity_provider
}