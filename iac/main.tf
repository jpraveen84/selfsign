terraform {
  required_version = ">= 1.0.1"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.20.1"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "15.11.0"
    }
  }
}

# Pass valid AWS profile

provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "http" {}
}