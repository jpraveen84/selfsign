module "sshKey" {
  count   = var.create_key == true ? 1 : 0
  source  = "./modules/ssh_key"
  keyname = var.key_name
}