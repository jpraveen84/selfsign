variable cidr {
    type   = string
}

variable newbits {
    type   = number
}

variable public_ip {
    type   = bool
}

variable vpc_id {
    type   = list
}

variable count_az {
    type   = number
}