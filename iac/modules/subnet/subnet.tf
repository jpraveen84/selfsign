data "aws_availability_zones" "available" {}

resource "aws_subnet" "public" {
  count = var.count_az
  vpc_id = var.vpc_id[0]
  cidr_block = cidrsubnet(var.cidr, var.newbits, count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
}
 
resource "aws_subnet" "private" {
  count = var.count_az
  vpc_id = var.vpc_id[0]
  cidr_block = cidrsubnet(var.cidr, var.newbits, count.index+var.count_az)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = false
}