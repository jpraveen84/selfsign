output "albDNS" {
    value = aws_lb.application_lb
}

output "tgARN" {
    value = aws_lb_target_group.target_group
}