resource "aws_eip" "nat" {
   vpc = true
}

resource "aws_nat_gateway" "nat" {
    allocation_id = aws_eip.nat.id
    subnet_id     = element(var.public_subnet,1)[0]
    tags          = {
        Name      = "nat-gw"
    }
}