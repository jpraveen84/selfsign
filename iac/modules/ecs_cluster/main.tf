resource "aws_ecs_cluster" "cluster" {
  name               = var.name
  capacity_providers = var.capacity_providers

  default_capacity_provider_strategy {
    capacity_provider = var.capacity_providers[0]
    weight            = "100"
  }
}