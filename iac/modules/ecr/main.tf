resource "aws_ecr_repository" "ecr-repo" {
  name                 = var.repo_name
  image_tag_mutability = var.tag_mut

  image_scanning_configuration {
    scan_on_push = var.scan
  }
}