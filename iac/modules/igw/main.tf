resource "aws_internet_gateway" "igw" {
    vpc_id = var.vpc_id[0]
    tags = {
       Name = "tf-igw"
  }
}