resource "aws_iam_policy" "iam-policy" {
  name        = var.policy_name
  path        = "/"
  policy      = file("./files/${var.policy_name}.json")
}