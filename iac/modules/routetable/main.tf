
resource "aws_eip" "nat" {
   vpc = true
}

resource "aws_nat_gateway" "nat" {
    allocation_id = aws_eip.nat.id
    subnet_id     = element(var.public_subnet,1)[0]
    tags          = {
        Name      = "nat-gw"
    }
}

resource "aws_internet_gateway" "igw" {
    vpc_id = var.vpc_id[0]
    tags = {
       Name = "tf-igw"
  }
}

resource "aws_route_table" "public" {

    vpc_id = var.vpc_id[0]

    route  {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw.id
    }
    tags = {
       Name = "public-rt"
  }
}


resource "aws_route_table_association" "public_subnet" {
  count          = var.count_az
  subnet_id      = element(var.public_subnet,count.index)[count.index]
  route_table_id = aws_route_table.public.id
}





resource "aws_route_table" "private" {
    vpc_id        = var.vpc_id[0]

    route  {
      cidr_block  = "0.0.0.0/0"
      gateway_id  = aws_nat_gateway.nat.id
    }
    tags = {
       Name       = "private-rt"
  }
  
}



resource "aws_route_table_association" "private_subnet" {
  count          = var.count_az
  subnet_id      = element(var.private_subnet,count.index)[count.index]
  route_table_id = aws_route_table.private.id
}