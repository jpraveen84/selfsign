variable vpc_id {
    type   = list
}

variable count_az {
    type   = number
}

variable public_subnet {
    type = list
}

variable private_subnet {
    type = list
}

# variable igw_id {
#    type = any
# }

# variable nat_id {
#    type = any
# }