locals {
    policy = var.policy_arn
}


resource "aws_iam_policy_attachment" "attachPolicy" {
  for_each = local.policy
  #for_each = toset(var.policy_arn)
  name = var.name
  roles      = var.role_name
  policy_arn = each.value
}