output "roleName" {
    value = aws_iam_role.iam-role.name
}

output "roleARN" {
    value = aws_iam_role.iam-role.arn
}