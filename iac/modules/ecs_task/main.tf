
resource "aws_ecs_task_definition" "service" {
  family = var.family
  #container_definitions = file("./files/${var.service_name}.json")
  container_definitions = templatefile("./files/${var.service_name}.json", {app_name = "selfsign", image_name = var.image_name})
  requires_compatibilities = var.requires_compatibilities
  execution_role_arn = var.role
  network_mode       = var.network_mode
  cpu                = var.cpu
  memory             = var.memory
}
