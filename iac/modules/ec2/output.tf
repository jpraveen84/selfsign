output "publicIp" {
    value = aws_instance.aws_ec2.*.public_ip
}