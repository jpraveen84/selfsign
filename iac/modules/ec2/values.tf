variable instance_type {}
variable ami {}
variable security_group_ids {}
variable key_name {}
variable tags {}
variable subnet {}
variable associate_public_ip_address {}
variable iam_instance_profile {}