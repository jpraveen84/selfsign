resource "aws_iam_instance_profile" "runner-profile" {
  name = "runner-profile"
  role = var.iam_instance_profile
}


resource "aws_instance" "aws_ec2" {
  ami           = var.ami
  instance_type = var.instance_type
  associate_public_ip_address = var.associate_public_ip_address
  key_name  = var.key_name
  subnet_id = element(var.subnet,1)[0]
  vpc_security_group_ids  = var.security_group_ids
  tags = var.tags
  iam_instance_profile = aws_iam_instance_profile.runner-profile.name
}