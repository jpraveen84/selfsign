output "security_group_ids" {
  value = [ for sg in aws_security_group.security_group : sg.id]
}