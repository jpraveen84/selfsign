resource "aws_ecs_service" "service" {
  name            = var.app_name
  cluster         = var.ecs_clusterID
  task_definition = var.ecs_taskarn
  desired_count   = 1

  network_configuration {
    subnets          = var.subnet
    assign_public_ip = true
    security_groups = var.ecsSG
  }

  load_balancer {
    target_group_arn = var.tg  
    container_name   = var.container_name
    container_port   = 80
  }
  capacity_provider_strategy {
    base              = 0
    capacity_provider = var.capacity_provider
    weight            = 100
  }
}