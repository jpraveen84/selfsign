locals {
  attach_policy = {
    "selfsign-ecstaskexecutionrole" = {
      policy_name = "selfsign-ecstaskexecutionpolicy"
      policy_arn = {
        policy = module.iam_policy.*.selfsign-ecstaskexecutionpolicy[0].policyARN[0]
      }


      #[module.iam_policy.*.selfsign-ecstaskexecutionpolicy[0].policyARN[0]]
    }
    "selfsignec2runnerrole" = {
      policy_arn = {
        ecr    = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess",
        s3     = "arn:aws:iam::aws:policy/AmazonS3FullAccess",
        ecs    = "arn:aws:iam::aws:policy/AmazonECS_FullAccess",
        ec2    = "arn:aws:iam::aws:policy/AmazonEC2FullAccess",
        cw     = "arn:aws:iam::aws:policy/CloudWatchFullAccess",
        iam    = "arn:aws:iam::aws:policy/IAMFullAccess",
        dynamo = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
      vpc = "arn:aws:iam::aws:policy/AmazonVPCFullAccess" }
    }
  }
}

module "attach_policy" {
  source     = "./modules/policy_attachment"
  for_each   = local.attach_policy
  name       = each.key
  role_name  = [each.key]
  policy_arn = each.value.policy_arn
  depends_on = [module.iam_role]
}