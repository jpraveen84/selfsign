data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}


locals {
  security_groups = {
    "http-https" = {
      name_prefix = "http-https"
      description = "http-https security group"
      ingress_rules = [
        {
          from_port   = 80
          to_port     = 80
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        },
        {
          from_port   = 443
          to_port     = 443
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      ]
      egress_rules = [
        {
          from_port   = 0
          to_port     = 0
          protocol    = "-1"
          cidr_blocks = ["0.0.0.0/0"]
        }
      ]
      tags = {
        Name = "http-https-sg"
      }
    },
    "ssh" = {
      name_prefix = "ssh"
      description = "ssh security group"
      ingress_rules = [
        {
          from_port   = var.ssh_port
          to_port     = var.ssh_port
          protocol    = "tcp"
          cidr_blocks = ["${chomp(data.http.myip.body)}/32"]
        }
      ]
      egress_rules = [
        {
          from_port   = 0
          to_port     = 0
          protocol    = "-1"
          cidr_blocks = ["0.0.0.0/0"]
        }
      ]
      tags = {
        Name = "ssh-sg"
      }
    }
  }

}

module "security_groups" {
  source          = "./modules/security_groups"
  security_groups = local.security_groups
  vpc_id          = toset(module.create_vpc[*].vpcId)
}