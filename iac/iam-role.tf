locals {
  role_name = {
    "ecr" = {
      name = "selfsign-ecrrole"
    }
    "ecs" = {
      name = "selfsign-ecstaskexecutionrole"
    }
    "ec2runner" = {
      name = "selfsignec2runnerrole"
    }
  }
}

module "iam_role" {
  source    = "./modules/iam_role"
  for_each  = local.role_name
  sts       = each.key
  role_name = each.value.name
}