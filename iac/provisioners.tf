resource "null_resource" "gitlab-ec2" {
  triggers = {
    public_ip   = module.ec2_instances.*.gitlab-runner[0].publicIp[0]
    file_change = md5(file("./files/install-runner.sh"))
  }
  provisioner "local-exec" {
    command = "pwd && ls"
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("${var.key_name}.pem")
    host        = module.ec2_instances.*.gitlab-runner[0].publicIp[0]

  }
  provisioner "file" {
    source      = "./files/install-runner.sh"
    destination = "/home/ubuntu/install-runner.sh"
  }
  provisioner "remote-exec" {
    inline = ["chmod +x /home/ubuntu/install-runner.sh",
      "./install-runner.sh ${var.gitlab_url} ${var.gitlab_token}"
    ]
  }
}


# resource "null_resource" "ssh-key" {
#   triggers = {
#     ssh-key = module.sshKey[0].keyPair
#   }
#   provisioner "local-exec" {
#        command = "cd files && mv -f ../${var.key_name}.pem ." 
#   }
# }