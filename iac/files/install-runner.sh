#!/bin/bash

if [ $# -ne 2 ]; then
  echo "Usage: $0 <GITLAB_URL> <REGISTRATION_TOKEN>"
  exit 1
fi

GITLAB_URL=$1
REGISTRATION_TOKEN=$2

# Add the GitLab Runner repository
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# Install the GitLab Runner package
sudo apt-get install gitlab-runner

# Register the Runner with GitLab
sudo gitlab-runner register \
  --non-interactive \
  --url $GITLAB_URL \
  --registration-token $REGISTRATION_TOKEN \
  --tag-list "ec2-runner" \
  --executor shell \
  --description "ec2-runner"


#install terraform
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common

wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg


gpg --no-default-keyring \
--keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
--fingerprint


echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
sudo tee /etc/apt/sources.list.d/hashicorp.list



sudo apt update

sudo apt-get install terraform

# Install necessary packages to allow apt to use a repository over HTTPS
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common

# Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Set up the Docker repository
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# Update the apt package index again
sudo apt-get update

# Install the latest version of Docker Engine and containerd
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

sudo usermod -aG docker gitlab-runner

sudo chmod 666 /var/run/docker.sock

sudo apt-get install awscli -y