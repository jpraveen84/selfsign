data "aws_availability_zones" "available" {}

module "routetable" {
  count_az = var.create_vpc == "true" ? length(data.aws_availability_zones.available.names) : 0
  source   = "./modules/routetable"
  vpc_id   = toset(module.create_vpc[*].vpcId)
  #   igw_id = toset(module.nat[*].igwId)
  #   nat_id = toset(module.nat[*].natId)
  public_subnet  = toset(module.subnet[*].vpc_public_subnets)
  private_subnet = toset(module.subnet[*].vpc_private_subnets)

}