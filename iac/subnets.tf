data "aws_availability_zones" "available_az" {}

module "subnet" {
  count_az = var.create_vpc == "true" ? length(data.aws_availability_zones.available_az.names) : 0
  source   = "./modules/subnet"
  vpc_id   = toset(module.create_vpc[*].vpcId)
  #   vpc_id =  [for vpc in module.create_vpc : vpc.id]
  cidr      = var.cidr
  newbits   = var.newbits
  public_ip = var.public_ip
}