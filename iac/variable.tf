variable "create_vpc" {
  type = string
}

variable "existing_vpc_id" {
  type = list(any)
}

variable "cidr" {
  type = string
}

variable "enable_dns_hostnames" {
  type = bool
}

variable "enable_dns_support" {
  type = bool
}

variable "app_name" {
  type = string
}

variable "env" {
  type = string
}

variable "newbits" {
  type = number
}

variable "public_ip" {
  type = bool
}

variable "key_name" {
  type = string
}

variable "create_key" {
  type = bool
}

variable "account_id" {
  type = string
}

variable "public_subnet" {}
variable "private_subnet" {}
variable "ssh_port" {}
variable "ssh_cidr" {}
variable "runner_instance_type" {}
variable "gitlab_url" {}
variable "gitlab_token" {} #gitlab runner token/ this value will be passed as a run time variable 
variable "selfsign" {}
variable "image_name" {}
