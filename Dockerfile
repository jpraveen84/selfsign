FROM node:slim

WORKDIR /app

COPY . .

EXPOSE 80/tcp

CMD [ "npm", "start" ]